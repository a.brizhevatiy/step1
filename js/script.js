$(window).on('load', function () {
        const serviceMenuItem = $('.services-menu-item');
        const serviceElems = $('.services-menu-advanced');
        serviceMenuItem.on('click', function (event) {
            event.preventDefault();
            serviceElems.removeClass('active');
            serviceMenuItem.removeClass('services-menu-item-active');
            const triangle = $('.service-menu-triangle');
            serviceMenuItem.find(triangle).removeClass('active');
            $(this).addClass('services-menu-item-active');
            $(this).find(triangle).addClass('active');
            const id = $(this).data('item');
            $(id).addClass('active');
        })
        // ---Amazing Work Gallery----
        const aWorkButton = $('.a-work-more-button');
        const aWorkImages = $('.a-work-gallery-img');
        const imageFilters = $('.a-work-image-filters-item');
        const loading = '<div class="cssload-container"><div class="cssload-whirlpool"></div></div>'
        let flag = false;
        let clickCount = 1;

        // Место издевательства над первой галлереей
        showPhoto();
        aWorkButton.on('click', () => {
            if (flag) return;
            flag = true;
            const tmp = aWorkButton.html();
            aWorkButton.html(loading)
            setTimeout(function () {
                showPhoto();
                if (clickCount !== 1) {
                    aWorkButton.html(tmp);
                }
                flag = false;
            }, 2000)
        });

        function showPhoto() {
            aWorkImages.each(function (i, item) {
                if (i < (clickCount * 12)) {
                    item.dataset.loaded = '1';
                }
            })
            const openId = $('.a-work-filter-active').data('images');
            const collection = openId === 'all' ? aWorkImages : $(`.a-work-gallery-img[data-item=${openId}]`);
            $(collection).each(function (i, item) {
                if (item.dataset.loaded === '1') {
                    $(item).slideDown(700);
                }
            });
            clickCount++;
            if (clickCount > 3) {
                clickCount = 1;
                $(aWorkButton).fadeOut();
            }
        }

        // место фильтрации картинок
        imageFilters.on('click', function () {
            if ($(this).hasClass('a-work-filter-active')) {
                return;
            }
            $(this).addClass('a-work-filter-active');
            $(imageFilters).not($(this)).removeClass('a-work-filter-active');
            const openId = $(this).data('images');
            const collection = openId === 'all' ? aWorkImages : $(`.a-work-gallery-img[data-item=${openId}]`);
            $(aWorkImages).fadeOut(500);
            setTimeout(() => {
                $(collection).each(function (i, item) {
                    if (item.dataset.loaded === '1') {
                        $(item).fadeIn(700);
                    }
                });
            }, 500)
        })

        const sayGallImg = $('.say-gallery-img');
        const sayGallImgBG = $('.say-gallery-img-bg');
        const sayCont = $('.say-zone');

        sayGallImg.on('click', function () {
            const img = './' + $(this).find($('img')).attr('src');
            $(sayGallImg).not(this).removeClass('gallery-active');
            $(sayGallImgBG).not($(this).parent()).removeClass('bg-active');
            $(this).addClass('gallery-active');
            $(this).parent().addClass('bg-active')
            $('.say-photo-in').css('background-image', `url(${img})`);
        })

        let listItem = 0;
        sayGallImg.on('click', function () {
            active($(this))
        })
        $('.button-left').on('click', prev);
        $('.button-right').on('click', next);

// Связывание картинок в карусели с основным изображением
        function active(item) {
            const img = './' + $(item).find($('img')).attr('src');
            const cont = $(item).data('content');
            listItem = ($(item).parent().index() - 1)
            $(sayGallImg).not(item).removeClass('gallery-active');
            $(sayGallImgBG).not($(item).parent()).removeClass('bg-active');
            $(item).addClass('gallery-active');
            $(item).parent().addClass('bg-active')
            $('.say-photo-in').css('background-image', `url(${img})`);
            $(sayCont).not($(cont)).removeClass('say-zone-active');
            $(cont).addClass('say-zone-active');
        }

// ----Функции для перелистывания в What People Say About theHam
        function next() {
            listItem++;
            if (listItem > sayGallImg.length - 1) {
                listItem = 0;
            }
            active(sayGallImg[listItem]);
        }

        function prev() {
            listItem--;
            if (listItem < 0) {
                listItem = 3;
            }
            active(sayGallImg[listItem]);
        }

        // место издевательства над Масонри
        const bestImgButton = $('.best-img-button');
        const bestAdvImgs = $('.best-advanced');
        bestImgButton.one('click', function () {
            bestImgButton.html(loading);
            bestImgButton.css({
                cursor: 'inherit',
            })
            setTimeout(function () {
                $(bestAdvImgs).fadeIn();
                $('.grid').masonry({
                    itemSelector: '.grid-item',
                    columnWidth: 99,
                    gutter: 1,
                });
                $(bestImgButton).fadeOut()
            }, 2000)
        })
        $('.grid').masonry({
            columnWidth: 99,
            itemSelector: '.grid-item',
            gutter: 1,
        });

        $('.br2').on('click', function (event) {
            const path = $($(this).parents()[1]).find($('img')).attr('src');
            const item = $(`<img src="${path}" alt="">`).css('max-width', '80vw');
            $(item).appendTo($('.show-photo'))
            $('.showPlace').fadeIn(300).css('display', 'flex');
        })

        $('.showPlace').on('click', function () {
            $(this).fadeOut();
            $(this).find($('img')).remove();
        })

    }
)